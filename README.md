The Euro Spatial Diffusion Observatory (ESDO) databases describe the socio-economic profil, residential location and origin X value of coins contained in the wallets of about 22,500 participants to Omnibus surveys conducted face-to-face in France (16 dates between March 2002 and December 2011), Belgium (December 2003) and Germany (December 2005).

For each country, a data table in .csv format reports the results of the survey waves and a meta-data file describes the variables and response patterns.
